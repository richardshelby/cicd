//import groovy.json.JsonBuilder

// have a common context to share across stages
BuildContext context

class BuildContext {
    String version = "null"
    String gitBranch = "null"
    String jenkinsUrl = "null"
//    int buildNumber = -1

    String getDisplayName() {
        return "[${gitBranch - "origin/"}] ${version}"
    }
}

pipeline {

    agent any

    // put this in the CI job or else hotfix will poll too
//    triggers {
//        pollSCM('* * * * *')
//    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
    }

    stages {

        stage('Process Build Context') {
            steps {
                script {
                    // create the build context
                    context = new BuildContext()
                }

                script {
                    // debug only - print Jenkins env
                    //sh('printenv')

                    // populate jenkins context variables
//                    context.gitUrl = sh(script: 'echo ${GIT_URL}', returnStdout: true).replaceAll("\n", "")
                    context.gitBranch = sh(script: 'echo ${GIT_BRANCH}', returnStdout: true).replaceAll("\n", "")
//                    context.gitCommit = sh(script: 'echo ${GIT_COMMIT}', returnStdout: true).replaceAll("\n", "")
                    context.jenkinsUrl = sh(script: 'echo ${HUDSON_URL}', returnStdout: true).replaceAll("\n", "")
//                    context.jenkinsVersion = sh(script: 'echo ${JENKINS_VERSION}', returnStdout: true).replaceAll("\n", "")
//                    context.buildUrl = sh(script: 'echo ${BUILD_URL}', returnStdout: true).replaceAll("\n", "")
//                    context.buildTag = sh(script: 'echo ${BUILD_TAG}', returnStdout: true).replaceAll("\n", "")
//                    context.buildNumber = sh(script: 'echo ${BUILD_NUMBER}', returnStdout: true).replaceAll("\n", "").toInteger()
//                    context.javaVersion = sh(script: 'echo ${JAVA_VERSION}', returnStdout: true).replaceAll("\n", "")
                }

                script {

                    // populate gradle context variables
                    context.version = sh(
                            script: './gradlew printVersion | grep "version:" | awk \'{print $2}\'',
                            returnStdout: true
                    ).replaceAll("\n", "")

                    currentBuild.displayName = context.getDisplayName()
                }
            }
        }

        stage('Compile + Unit Tests') {
            steps {
                sh './gradlew test jacocoTestReport --console=plain --stacktrace'
            }
            post {
                always {
                    junit '**/build/test-results/test/TEST-*.xml'
                }
            }
        }

        stage('Tag Repo') {
            when {
                expression {
                    return (context.gitBranch ==~ /.*release\/.*|.*master/)
                }
            }
            steps {
                sshagent(['c77f718f-2ad8-4025-a8a6-ff5f44e6de07']) {
                    sh 'git config user.email "no-email@nowhere.com"'
                    sh "git config user.name \"jenkins @${context.jenkinsUrl}\""

                    sh "git tag ${context.version}"
                    sh "git push origin ${context.version}"
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
