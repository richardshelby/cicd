package com.kyriba.hub.samples;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HelloTest {

    @Test
    public void helloWorld() {
        assertEquals("hello", new Hello().helloWorld());
    }
}